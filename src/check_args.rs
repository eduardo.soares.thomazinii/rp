use std::process;

pub fn get_filename(args: Vec<String>)->String{
    if args.len() == 1{
        println!("No CSV file provided, please provide the CSV File Path");
        process::exit(1);
    }
    if args.len() > 2{
        println!("More arguments than expected, please provided only the CSV File Path");
        process::exit(1);
    }
    return args[1].to_string();
}
