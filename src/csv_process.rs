use crate::{OperationT,OperationType};
use std::fs::File;
use csv::{ReaderBuilder,Trim};


pub fn parse_csv(csv_file: &String)->Vec<OperationT>{
    let transaction_list: &mut Vec<OperationT> = &mut Vec::new();

    let file = File::open(&csv_file).unwrap();
    let mut reader = ReaderBuilder::new()
        .trim(Trim::All)
        .flexible(true)
        .from_reader(file);
    for result in reader.deserialize::<OperationT>() {
        let tr: OperationT = result.unwrap();
        transaction_list.push(tr);
    }
    return transaction_list.to_vec();
 }

 
pub fn process_tansactions(data:&mut Vec<OperationT>)-> (Vec<&OperationT>,Vec<&OperationT>,Vec<&OperationT>,Vec<&OperationT>){

    let mut deposit_withdraw_transactions: Vec<&OperationT> = Vec::new();
    let mut dispute_transaction: Vec<&OperationT> = Vec::new();
    let mut resolve_transaction: Vec<&OperationT> = Vec::new();
    let mut chargeback_transaction: Vec<&OperationT> = Vec::new();

    for trx in data.iter(){
        match trx.t_type {
            OperationType::Deposit=> {
                deposit_withdraw_transactions.push(trx);
            },
            OperationType::Withdrawal=> {
                deposit_withdraw_transactions.push(trx);
            },
            OperationType::Dispute=> {
                dispute_transaction.push(trx);
            },
            OperationType::Resolve => {
                resolve_transaction.push(trx);
            }
            OperationType::Chargeback => {
                chargeback_transaction.push(trx);
            }
        }
    }
    return (deposit_withdraw_transactions,dispute_transaction,resolve_transaction,chargeback_transaction);
}
 

