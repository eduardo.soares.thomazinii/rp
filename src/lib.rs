use rustdecimal::Decimal;
use serde::Deserialize;
use std::error::Error;
pub mod csv_process;
pub mod wallet;
pub mod check_args;

#[derive(Debug, Clone)]
pub struct AccStatus {
   pub client:   u16,
   pub total_available:    Decimal,
   pub held:    Decimal,
   pub total_amount:    Decimal,
   pub is_locked:  bool
}

pub trait MyWallet {
   fn process_ops(&mut self,trans: &OperationT) -> Result<(), Box<dyn Error>>;
   fn extract_value(&self, client_id: u16) -> Result<Decimal, Box<dyn Error>>;
   fn all_amount(&self, client_id: u16) -> Result<Decimal, Box<dyn Error>>;
   fn check_trans(&self, trans: &OperationT) -> Result<(), Box<dyn Error>>;
   fn show_value_onhold(&self, client_id: u16) -> Result<Decimal, Box<dyn Error>>;
   fn parse_transact(&mut self, withdraw_deposits: Vec<&OperationT>, disputes: Vec<&OperationT>, resolves: Vec<&OperationT>, chargebacks: Vec<&OperationT>)-> String;
}

pub fn process(){}


#[derive(Debug, Deserialize,Clone)]
pub struct OperationT {
    #[serde(rename = "type")]
    pub t_type: OperationType,
    pub client: Option<u16>,
    pub tx: Option<u32>,
    pub amount: Option<Decimal>
 }

 #[derive(Debug, Deserialize,Clone,PartialEq)]
 #[serde(rename_all = "lowercase")]
 pub enum OperationType {
    Deposit,
    Withdrawal,
    Dispute,
    Resolve,
    Chargeback
 }

 
