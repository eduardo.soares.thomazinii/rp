use std::env;
use challenge::{csv_process::{parse_csv,process_tansactions},wallet::DefinitionMyWallet,OperationT,MyWallet,check_args};
fn main() {
    let mut wallet = DefinitionMyWallet::default();
    let mut transaction_list: Vec<OperationT>;
    let args: Vec<String> = env::args().collect();
    let csv_file = check_args::get_filename(args);
    transaction_list = parse_csv(&csv_file);
    let (deposit_withdraw_transactions,dispute_transaction,resolve_transaction,chargeback_transaction) = process_tansactions(&mut transaction_list);
    println!("{}",wallet.parse_transact(deposit_withdraw_transactions, dispute_transaction, resolve_transaction, chargeback_transaction));
}

