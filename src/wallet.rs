use crate::{AccStatus, MyWallet,OperationT,OperationType};
use std::collections::{HashMap,hash_map::Entry::{Occupied,Vacant}};
use std::{error::Error};
use rustdecimal::Decimal;

const ERROR_NO_TRANS_ID: &str = "No Transaction ID Found";
const ERROR_NO_CLIENT_ID: &str = "No Transaction ID Found";
const TRANSACT_EXISTENT: &str = "Transaction previously added";
const NO_AMOUNT: &str = "No Amount found";
const NO_AMOUNT_FOR_CLIENT_ID: &str = "No amount for the client id";
const ACC_NOT_INIT: &str = "Account not created";
const NO_IDS: &str = "No identification for the account found";

#[derive(Debug)]
pub struct DefinitionMyWallet {
    pub map_client_id: HashMap<u16, AccStatus>,
    pub map_trans_id: HashMap<u32, OperationT>
}

impl Default for DefinitionMyWallet {
    fn default() -> Self {
        Self {
            map_client_id: HashMap::new(),
            map_trans_id: HashMap::new()
        }
    }
}


impl DefinitionMyWallet {

    fn create_account(client_id: u16) -> AccStatus {
        let new_account = AccStatus { 
            client: client_id, total_available: Decimal::new(0,0), held: Decimal::new(0,0), is_locked: false, total_amount: Decimal::new(0,0)};
        return new_account;
    }


    pub fn withdrawl(&mut self,transact: &OperationT) -> Result<(), Box<dyn Error>> {
        let client_id = match transact.client {
            Some(var) => var,
            None => return Err(ERROR_NO_CLIENT_ID.into()),
        };
        let transact_id = match transact.tx {
            Some(var) => var,
            None => return Err(ERROR_NO_TRANS_ID.into()),
        };
        let transact_amount = match transact.amount {
            Some(var) => {var},
            None => return Err(NO_AMOUNT.into()),
        };
        match self.map_trans_id.entry(transact_id) {
            Occupied(_) => {
                return Err(TRANSACT_EXISTENT.into());
            },
            Vacant(comparison) => {
                comparison.insert(transact.clone());
            }
        }
        match self.map_client_id.entry(client_id) {
            Occupied(mut comparison) => {
                let mut status = comparison.get_mut();
                let amount_available = status.total_available;
                if transact_amount > amount_available {
                    return Err(NO_AMOUNT.into());
                }
                status.total_available = amount_available - transact_amount;
            },
            Vacant(_) => {
                return Err(NO_AMOUNT_FOR_CLIENT_ID.into());
            }
        }
        return Ok(());
    }


    fn resolve(&mut self,trans: &OperationT) -> Result<(), Box<dyn Error>> {
        let client_id = match trans.client {
            Some(var) => var,
            None => return Err(ERROR_NO_CLIENT_ID.into()),
        };
        let transact_id = match trans.tx {
            Some(var) => var,
            None => return Err(ERROR_NO_TRANS_ID.into()),
        };
        match self.map_trans_id.entry(transact_id) {
            Occupied(client_transaction) => {
                let ct = client_transaction.get();
                let clientid = match ct.client {
                    Some(var) => var,
                    None => 0
                };
                if clientid == client_id {  
                    match self.map_client_id.entry(client_id) {
                        Occupied(mut stauts_acc) => {
                            let mut client_st = stauts_acc.get_mut();
                            let amount_value = match ct.amount {
                                Some(var) => var,
                                None => Decimal::new(0,0),
                            };
                            if amount_value <= client_st.held {
                                client_st.total_available = client_st.total_available + amount_value;
                                client_st.held = client_st.held - amount_value;
                            }
                        },
                        Vacant(_) => {}
                    }
                }
            }
            Vacant(_) => {}
        }
        return Ok(());
    }

    fn dispute(&mut self, transact: &OperationT) -> Result<(), Box<dyn Error>> {
        let client_id = match transact.client {
            Some(var) => var,
            None => return Err(ERROR_NO_CLIENT_ID.into()),
        };
        let transact_id = match transact.tx {
            Some(var) => var,
            None => return Err(ERROR_NO_TRANS_ID.into()),
        };
        match self.map_trans_id.entry(transact_id) {
            Occupied(cli_trans) => {
                let cltx = cli_trans.get();
                let clientid = match cltx.client {
                    Some(var) => var,
                    None => 0
                };
                if clientid == client_id { 
                    match self.map_client_id.entry(client_id) {
                        Occupied(mut stacc) => {
                            let mut status_acc = stacc.get_mut();
                            let value_amount = match cltx.amount {
                                Some(v) => v,
                                None => Decimal::new(0,0),
                            };
                            if value_amount <= status_acc.total_available {
                                status_acc.total_available = status_acc.total_available - value_amount;
                                status_acc.held = status_acc.held + value_amount;
                            }
                        },
                        Vacant(_) => {}
                    }
                }
            }
            Vacant(_) => {}
        }
        return Ok(());
    }

    fn chargeback(&mut self,transact: &OperationT) -> Result<(), Box<dyn Error>> {
        let client_id = match transact.client {
            Some(var) => var,
            None => return Err(ERROR_NO_CLIENT_ID.into()),
        };
        let transact_id = match transact.tx {
            Some(var) => var,
            None => return Err(ERROR_NO_TRANS_ID.into()),
        };
        match self.map_trans_id.entry(transact_id) {
            Occupied(client_transaction) => {
                let client_trans = client_transaction.get();
                let clid = match client_trans.client {
                    Some(var) => var,
                    None => 0
                };
                if clid == client_id {  
                    match self.map_client_id.entry(client_id) {
                        Occupied(mut acc_status) => {
                            let mut status_account_mut = acc_status.get_mut();
                            let cat_amount_val = match client_trans.amount {
                                Some(var) => var,
                                None => Decimal::new(0,0),
                            };
                            if cat_amount_val <= status_account_mut.total_available { 
                                status_account_mut.total_available = status_account_mut.total_available - cat_amount_val;
                                status_account_mut.is_locked = true;   
                            }
                        },
                        Vacant(_) => {}
                    }
                } 
            }
            Vacant(_) => {}
        }
        return Ok(());
    }

    pub fn deposit(&mut self, transact: &OperationT) -> Result<(), Box<dyn Error>> {
        let transact_id = match transact.tx {
            Some(var) => var,
            None => return Err(ERROR_NO_TRANS_ID.into()),
        };
        let transact_amount = match transact.amount {
            Some(var) => {var},
            None => {Decimal::new(0,0)},
        };
        let client_id = match transact.client {
            Some(var) => var,
            None => return Err(ERROR_NO_CLIENT_ID.into()),
        };
        match self.map_trans_id.entry(transact_id) {
            Occupied(_) => {
                return Err(TRANSACT_EXISTENT.into());
            },
            Vacant(entry) => {
                entry.insert(transact.clone());
            }
        }
        match self.map_client_id.entry(client_id) {
            Occupied(mut comparison) => {
                let mut status = comparison.get_mut();
                let amount_available = status.total_available;
                status.total_available = amount_available + transact_amount;
            },
            Vacant(comparison) => {
                let mut status = Self::create_account(client_id);
                status.total_available = transact_amount;
                comparison.insert(status);
            }
        }
        return Ok(());
    }
}

impl MyWallet for DefinitionMyWallet {
    fn process_ops(&mut self, transact: &OperationT) -> Result<(), Box<dyn Error>> {
        let client_id = match transact.client {
            Some(var) => var,
            None => return Err(ERROR_NO_TRANS_ID.into()),
        };
        let status = self.map_client_id.get(&client_id);
        match status {
            Some(client_status) => {
                if client_status.is_locked {
                    return Ok(()); 
                }
            },
            None => {} 
        };
        Self::check_trans(&self,transact).unwrap();
        match transact.t_type {
            OperationType::Deposit => {
                self.deposit(transact)?;
            },
            OperationType::Withdrawal => {
                match self.withdrawl(transact) {
                    Ok(_) => {},
                    Err(_e) => {
                    }
                } 
            },
            OperationType::Dispute => {
                self.dispute(transact)?; 
            },
            OperationType::Resolve => {
                self.resolve(transact)?; 
            }
            OperationType::Chargeback => {
                self.chargeback(transact)?; 
            },
        };
        let _new_account_status = self.map_client_id.get(&client_id);    
        return Ok(());
    }

    fn all_amount(&self, clsid: u16) -> Result<Decimal, Box<dyn Error>> {
        let client = match self.map_client_id.get(&clsid) {
            Some(var) => var,
            None => { return Err(ACC_NOT_INIT.into())}
        };
        let all = client.total_available + client.held;
        return Ok(all);
    }

    fn extract_value(&self, client_id: u16) -> Result<Decimal, Box<dyn Error>> {
        let amount_av = match self.map_client_id.get(&client_id) {
            Some(var) => var.total_available,
            None => { return Err(ACC_NOT_INIT.into())}
        };
        return Ok(amount_av)
    }
    
    fn show_value_onhold(&self, client_id: u16) -> Result<Decimal, Box<dyn Error>> {
        let amount_av = match self.map_client_id.get(&client_id) {
            Some(var) => var.total_available,
            None => { return Err(ACC_NOT_INIT.into())}
        };
        return Ok(amount_av)
    }
    

    fn check_trans(&self, transact: &OperationT) -> Result<(), Box<dyn Error>> {
        if transact.client == None || transact.tx == None {
            return Err(NO_IDS.into());
        }
        if transact.t_type == OperationType::Dispute || transact.t_type == OperationType::Resolve || transact.t_type == OperationType::Chargeback {
            if transact.amount != None {
                return Err(NO_IDS.into());
            }
        } else {
            if transact.amount == None {
                return Err(NO_IDS.into());
            }
        }
        Ok(())
    }

    fn parse_transact(&mut self, withdraw_deposits: Vec<&OperationT>, disputes: Vec<&OperationT>, resolves: Vec<&OperationT>, chargebacks: Vec<&OperationT>) -> String{
        for transaction in withdraw_deposits {
            self.process_ops(&transaction).unwrap();
        }
        for transaction in disputes {
           self.process_ops(&transaction).unwrap();
        }
        for transaction in resolves {
           self.process_ops(&transaction).unwrap()
        }
        for transaction in chargebacks {
           self.process_ops(&transaction).unwrap();
        }
        let get_clients = &self.map_client_id;
        let mut output:String = String::new();
        output.push_str("client, available, held, total, locked\n");
        for (client_id, status) in get_clients{
            let all_value = status.total_available + status.held;
            //output += "{},{},{},{},{}",client_id,status.total_available,status.held,all_value,status.is_locked;
            output.push_str(&format!("{},{},{},{},{}\n",client_id,status.total_available,status.held,all_value,status.is_locked));

        }
        return output;
    }
    
}
