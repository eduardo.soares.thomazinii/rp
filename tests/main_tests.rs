use csv::{ReaderBuilder,Trim};
use challenge::MyWallet;
use challenge::{csv_process::{process_tansactions},wallet::DefinitionMyWallet,OperationT};

#[test]
fn main_sample_test() {
    let csv_sample: &str = "type, client, tx, amount\ndeposit, 1, 1, 1.0\ndeposit, 2, 2, 2.0\ndeposit, 1, 3, 2.0\nwithdrawal, 1, 4, 1.5\nwithdrawal, 2, 5, 3.0";
    let expected: &str = "client, available, held, total, locked\n1,1.5,0,1.5,false\n2,2,0,2,false\n";
    let mut wallet = DefinitionMyWallet::default();
    let mut transaction_list: Vec<OperationT> = Vec::new();
    let mut reader = ReaderBuilder::new()
        .trim(Trim::All)
        .flexible(true)
        .from_reader(csv_sample.as_bytes());
    for result in reader.deserialize::<OperationT>() {
        let tr: OperationT = result.unwrap();
        transaction_list.push(tr);
    }
    let (deposit_withdraw_transactions,dispute_transaction,resolve_transaction,chargeback_transaction) = process_tansactions(&mut transaction_list);
    assert_eq!(expected,wallet.parse_transact(deposit_withdraw_transactions, dispute_transaction, resolve_transaction, chargeback_transaction));
}

#[test]
fn main_four_decimal_test() {
    let csv_sample: &str = "type, client, tx, amount\ndeposit, 1, 1, 1.2343\ndeposit, 2, 2, 2.2343\ndeposit, 1, 3, 2.2334\nwithdrawal, 1, 4, 1.5123\nwithdrawal, 2, 5, 3.2343";
    let mut wallet = DefinitionMyWallet::default();
    let mut transaction_list: Vec<OperationT> = Vec::new();
    let mut reader = ReaderBuilder::new()
        .trim(Trim::All)
        .flexible(true)
        .from_reader(csv_sample.as_bytes());
    for result in reader.deserialize::<OperationT>() {
        let tr: OperationT = result.unwrap();
        transaction_list.push(tr);
    }
    let (deposit_withdraw_transactions,dispute_transaction,resolve_transaction,chargeback_transaction) = process_tansactions(&mut transaction_list);
    let expected = "client, available, held, total, locked\n2,2.2343,0,2.2343,false\n1,1.9554,0,1.9554,false\n";
    assert_eq!(expected,wallet.parse_transact(deposit_withdraw_transactions, dispute_transaction, resolve_transaction, chargeback_transaction));
}

